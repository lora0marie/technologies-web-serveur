package wishList.model;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface Model {
    String createListe(String nom, String description);
    void deleteListe(int id);
    void modifierListe(int id, String nom, String description);
    String createElement(int id, String nom, String description);
    void deleteElement(int id);
    void modifierElement(int id, String nom, String description);
    List<Liste> getAllListe();
    List<Liste> getListe(int id);
    List<Element> getElementListe(int id);
    List<Element> getElement(int id);
    int getElementIdListe(int id_E);
}