package wishList.model;

import org.sql2o.Connection;
import org.sql2o.Sql2o;

import wishList.model.Model;
import wishList.model.Liste;
import wishList.model.Element;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class Sql2oModel implements Model {

    private Sql2o sql2o;

    public Sql2oModel(Sql2o sql2o) {
        this.sql2o = sql2o;
    }

    @Override
    public String createListe(String nom, String description) {
        try (Connection conn = sql2o.beginTransaction()) {
            conn.createQuery("insert into LISTE(NOM, DESCRIPTION) VALUES (:nom, :description);")
                    .addParameter("nom", nom)
                    .addParameter("description", description)
                    .executeUpdate();
            conn.commit();
            return nom;
        }
    }

    @Override
    public void deleteListe(int id) {
        try (Connection conn = sql2o.beginTransaction()) {
            conn.createQuery("delete from LISTE where ID_L=:id_l;")
                    .addParameter("id_l", id)
                    .executeUpdate();
            conn.commit();
        }
    }

    @Override
    public void modifierListe(int id, String nom, String description) {
        try (Connection conn = sql2o.beginTransaction()) {
            conn.createQuery("update LISTE set NOM=:nom , DESCRIPTION=:description where ID_L=:id;")
                    .addParameter("nom", nom)
                    .addParameter("description", description)
                    .addParameter("id", id)
                    .executeUpdate();
            conn.commit();
        }
    }

    @Override
    public String createElement(int liste, String nom, String description) {
        try (Connection conn = sql2o.open()) {
            conn.createQuery("insert into ELEMENT(NOM, DESCRIPTION, LISTE) VALUES (:nom, :description, :liste_id)")
                    .addParameter("nom", nom)
                    .addParameter("description", description)
                    .addParameter("liste_id", liste)
                    .executeUpdate();
            conn.commit();
            return nom;
        }
    }

    @Override
    public void deleteElement(int id) {
        try (Connection conn = sql2o.beginTransaction()) {
            conn.createQuery("delete from ELEMENT where ID_E=:id")
                    .addParameter("id", id)
                    .executeUpdate();
            conn.commit();
        }
    }

    @Override
    public void modifierElement(int id, String nom, String description) {
        try (Connection conn = sql2o.beginTransaction()) {
            Date d = new Date();
            conn.createQuery("update ELEMENT set NOM=:nom, DESCRIPTION=:description, DATE_MODIFICATION=:date where ID_E=:id")
                    .addParameter("nom", nom)
                    .addParameter("description", description)
                    .addParameter("date", d)
                    .addParameter("id", id)
                    .executeUpdate();
            conn.commit();
        }
    }

    @Override
    public List<Liste> getAllListe() {
        try (Connection conn = sql2o.open()) {
            List<Liste> listes = conn.createQuery("select * from LISTE")
                    .executeAndFetch(Liste.class);
            return listes;
        }
    }

    @Override
    public List<Liste> getListe(int id) {
        try (Connection conn = sql2o.open()) {
            List<Liste> liste = conn.createQuery("select * from LISTE where ID_L=:id")
                    .addParameter("id", id)
                    .executeAndFetch(Liste.class);
            return liste;
        }
    }

    @Override
    public List<Element> getElementListe(int id) {
        try (Connection conn = sql2o.open()) {
            List<Element> elements = conn.createQuery("select * from ELEMENT where LISTE=:id")
                    .addParameter("id", id)
                    .executeAndFetch(Element.class);
            return elements;
        }
    }

    @Override
    public List<Element> getElement(int id) {
        try (Connection conn = sql2o.open()) {
            List<Element> element = conn.createQuery("select * from ELEMENT where ID_E=:id")
                    .addParameter("id", id)
                    .executeAndFetch(Element.class);
            return element;
        }
    }

    @Override
    public int getElementIdListe(int id_E) {
        try (Connection conn = sql2o.open()) {
            int id_liste = conn.createQuery("select LISTE from ELEMENT where ID_E=:id")
                    .addParameter("id", id_E)
                    .executeScalar(Integer.class);
            return id_liste;
        }
    }
}
