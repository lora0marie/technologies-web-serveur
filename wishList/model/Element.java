package wishList.model;

import java.util.Date;
import java.util.UUID;

public class Element {
    private int ID_E;
    private String NOM;
    private String DESCRIPTION;
    private Date DATE_CREATION;
    private Date DATE_MODIFICATION;
    private int LISTE;

    public int getID_E() {
        return ID_E;
    }

    public String getNOM() {
        return NOM;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public Date getDATE_CREATION() {
        return DATE_MODIFICATION;
    }

    public Date getDATE_MODIFICATION() {
        return DATE_MODIFICATION;
    }

    public int getLISTE() {
        return LISTE;
    }
}
