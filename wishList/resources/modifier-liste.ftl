<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		<#include "bootstrap.min.css">
		body{
			margin: 15px;
		}
		form{
			margin-top: 15px;
		}
	</style>
</head>
<body>
	<h1>modifier la liste : ${liste[0].NOM}</h1>
	<a class="btn btn-secondary" href="/" role="button">Page d'accueil</a>
	<form action="/liste/${liste[0].ID_L}/modifier" method="post">
		<div class="form-group">
			<label for="nomListe">Nom de la liste</label>
			<input type="text" class="form-control" name="nomListe" placeholder="${liste[0].NOM}">
		</div>
		<div class="form-group">
			<label for="descriptionListe">Description de la liste</label>
			<input type="text" class="form-control" name="descriptionListe" placeholder="${liste[0].DESCRIPTION}">
		</div>
		<button type="submit" class="btn btn-primary">Valider</button>
	</form>
</body>
</html>