<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		<#include "bootstrap.min.css">
		body{
			margin: 15px;
		}
		form{
			margin-top: 15px;
		}
	</style>
</head>
<body>
	<h1>Ajouter un element a la liste : ${liste[0].NOM}</h1>
	<a class="btn btn-secondary" href="/" role="button">Page d'accueil</a>
	<a class="btn btn-secondary" href="/liste/${liste[0].ID_L}" role="button">Liste des elements</a>
	<form action="/liste/${liste[0].ID_L}/ajouter" method="post">
		<div class="form-group">
			<label for="nomElement">Nom de l'élèment</label>
			<input type="text" class="form-control" name="nomElement" placeholder="Entrer nom">
		</div>
		<div class="form-group">
			<label for="descriptionElement">Description de la élèment</label>
			<input type="text" class="form-control" name="descriptionElement" placeholder="Entrer description">
		</div>
		<button type="submit" class="btn btn-primary">Valider</button>
	</form>
</body>
</html>