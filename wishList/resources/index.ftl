<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		<#include "bootstrap.min.css">
		body{
			margin: 15px;
		}
		table{
			margin-top: 15px;
		}
		form{
			display: inline;
		}
	</style>
</head>
<body>
	<h1>${titre}</h1>
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Nom</th>
				<th scope="col">Description</th>
				<th scope="col">Options</th>
			</tr>
		</thead>
		<tbody>
			<#list listes as liste>
				<tr>
					<td>${liste.NOM}</td>
					<td>${liste.DESCRIPTION}</td>
					<td>
						<a class="btn btn-info" href="/liste/${liste.ID_L}" role="button">voir plus</a>
						<a class="btn btn-warning" href="/liste/${liste.ID_L}/modifier" role="button">Modifier</a>
						<form action="/supprimer/liste/${liste.ID_L}" method="POST">
	  						<button type="submit" class="btn btn-danger">Supprimer</button>
						</form>
					</td>
				</tr>
			</#list>
	  </tbody>
	</table>
	<a class="btn btn-success" href="/liste/ajouter" role="button">Ajouter une liste</a>
</body>
</html>