<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		<#include "bootstrap.min.css">
		body{
			margin: 15px;
		}
		table{
			margin-top: 15px;
		}
		form{
			display: inline;
		}
	</style>
</head>
<body>
	<h1>${liste[0].NOM}</h1>
	<a class="btn btn-secondary" href="/" role="button">Page d'accueil</a>
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Nom</th>
				<th scope="col">Description</th>
				<th scope="col">Options</th>
			</tr>
		</thead>
		<tbody>
			<#list elements as element>
			<tr>
				<td>${element.NOM}</td>
				<td>${element.DESCRIPTION}</td>
				<td>
					<a class="btn btn-warning" href="/element/${element.ID_E}/modifier" role="button">Modifier</a>
					<form action="/supprimer/element/${element.ID_E}" method="POST">
	  					<button type="submit" class="btn btn-danger">Supprimer</button>
					</form>
				</td>
			</tr>
			</#list>
	  </tbody>
	</table>
	<a class="btn btn-success" href="/liste/${liste[0].ID_L}/ajouter" role="button">Ajouter un element</a>
</body>
</html>