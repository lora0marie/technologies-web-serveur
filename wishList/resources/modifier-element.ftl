<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		<#include "bootstrap.min.css">
		body{
			margin: 15px;
		}
		form{
			margin-top: 15px;
		}
	</style>
</head>
<body>
	<h1>modifier l'élèment : ${element[0].NOM}</h1>
	<a class="btn btn-secondary" href="/" role="button">Page d'accueil</a>
	<a class="btn btn-secondary" href="/liste/${element[0].LISTE}" role="button">Liste des elements</a>
	<form action="/element/${element[0].ID_E}/modifier" method="post">
		<div class="form-group">
			<label for="nomElement">Nom de l'élèment</label>
			<input type="text" class="form-control" name="nomElement" placeholder="${element[0].NOM}">
		</div>
		<div class="form-group">
			<label for="descriptionElement">Description de l'élèment</label>
			<input type="text" class="form-control" name="descriptionElement" placeholder="${element[0].DESCRIPTION}">
		</div>
		<button type="submit" class="btn btn-primary">Valider</button>
	</form>
</body>
</html>