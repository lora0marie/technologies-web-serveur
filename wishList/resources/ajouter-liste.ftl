<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		<#include "bootstrap.min.css">
		body{
			margin: 15px;
		}
		form{
			margin-top: 15px;
		}
	</style>
</head>
<body>
	<h1>Ajouter une liste</h1>
	<a class="btn btn-secondary" href="/" role="button">Page d'accueil</a>
	<form action="/liste/ajouter" method="post">
		<div class="form-group">
			<label for="nomListe">Nom de la liste</label>
			<input type="text" class="form-control" name="nomListe" placeholder="Entrer nom">
		</div>
		<div class="form-group">
			<label for="descriptionListe">Description de la liste</label>
			<input type="text" class="form-control" name="descriptionListe" placeholder="Entrer description">
		</div>
		<button type="submit" class="btn btn-primary">Valider</button>
	</form>
</body>
</html>