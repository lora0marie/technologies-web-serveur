package wishList;

import static spark.Spark.*;
import spark.Request;
import spark.TemplateEngine;
import spark.ModelAndView;
import freemarker.template.*;

import org.sql2o.Sql2o;
import org.sql2o.converters.UUIDConverter;
import org.sql2o.quirks.PostgresQuirks;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import wishList.model.Model;
import wishList.model.Sql2oModel;

public class Controleur {
    
    private static final Logger logger = Logger.getLogger(Controleur.class.getCanonicalName());

    public static void main( String[] args) {
        
        Sql2o sql2o = new Sql2o("jdbc:h2:~/test", "sa", "");

    	Model model = new Sql2oModel(sql2o);

    	// accueil
		get("/", (req, res) -> {
		    Map<String, Object> attributes = new HashMap<>();
		    attributes.put("titre", "Liste de trucs comme on le veut");
		    attributes.put("listes", model.getAllListe());
		    return new FreeMarkerEngine().render(
		        new ModelAndView(attributes, "resources/index.ftl")
		    );
		});

		// ajouter une liste
		get("/liste/ajouter", (req, res) -> {
		    Map<String, Object> attributes = new HashMap<>();
		    return new FreeMarkerEngine().render(
		        new ModelAndView(attributes, "resources/ajouter-liste.ftl")
		    );
		});

		// (execution) ajouter une liste
		post("/liste/ajouter", (req, res) -> {
			String nom = req.queryParams("nomListe");
			String description = req.queryParams("descriptionListe");
			model.createListe(nom, description);
			res.redirect("/");
		    return "";
		});

		// une liste spécifique (id)
		get("/liste/:id", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
		    Map<String, Object> attributes = new HashMap<>();
		    attributes.put("liste", model.getListe(id));
		    attributes.put("elements", model.getElementListe(id));
		    return new FreeMarkerEngine().render(
		        new ModelAndView(attributes, "resources/liste.ftl")
		    );
		});

		// modifier une liste (id)
		get("/liste/:id/modifier", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
		    Map<String, Object> attributes = new HashMap<>();
		    attributes.put("liste", model.getListe(id));
		    return new FreeMarkerEngine().render(
		        new ModelAndView(attributes, "resources/modifier-liste.ftl")
		    );
		});

		// (execution) modifier une liste (id)
		post("/liste/:id/modifier", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
			String nom = req.queryParams("nomListe");
			String description = req.queryParams("descriptionListe");
			model.modifierListe(id, nom, description);
			res.redirect("/");
		    return "";
		});

		// delete cette liste
		post("/supprimer/liste/:id", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
			model.deleteListe(id);
			res.redirect("/");			
			return "";
		});

		// ajouter un element a une liste spécifique (id)
		get("/liste/:id/ajouter", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
			Map<String, Object> attributes = new HashMap<>();
		    attributes.put("liste", model.getListe(id));
		    return new FreeMarkerEngine().render(
		        new ModelAndView(attributes, "resources/ajouter-element.ftl")
		    );
		});

		// (execution) ajouter un element a une liste spécifique (id)
		post("/liste/:id/ajouter", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
			String nom = req.queryParams("nomElement");
			String description = req.queryParams("descriptionElement");
			model.createElement(id, nom, description);
			res.redirect("/liste/"+id);
		    return "";
		});

		// modifier une liste (id)
		get("/element/:id/modifier", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
		    Map<String, Object> attributes = new HashMap<>();
		    attributes.put("element", model.getElement(id));
		    return new FreeMarkerEngine().render(
		        new ModelAndView(attributes, "resources/modifier-element.ftl")
		    );
		});

		// (execution) modifier une liste (id)
		post("/element/:id/modifier", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
			String nom = req.queryParams("nomElement");
			String description = req.queryParams("descriptionElement");
			if(!nom.isEmpty() && !description.isEmpty()){
				System.out.println("pas vide");
				model.modifierElement(id, nom, description);
				int liste = model.getElementIdListe(id);
				res.redirect("/liste/"+liste);
			}
			else{
				System.out.println("vide");
				res.redirect("/element/"+id+"/modifier");
			}
			
			// voir pour faire redirection vers la liste d'ou est issue l'élèment

		    return "";
		});

		// delete cet element
		post("/supprimer/element/:id", (req, res) -> {
			Integer id = Integer.valueOf(req.params(":id"));
			int liste = model.getElementIdListe(id);
			model.deleteElement(id);
			res.redirect("/liste/"+liste);
			return "";
		});
    }
}