JFLAGS = -g
JC = javac -cp .:wishList/lib/*
JVM = java -cp .:wishList/lib/*
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java
CLASSES = \
	wishList/*.java\
	wishList/model/*.java
MAIN = wishList/Controleur

default: classes

classes: $(CLASSES:.java=.class)

run: $(MAIN).class
	$(JVM) $(MAIN)

clean:
	$(RM) wishList/*.class
	$(RM) wishList/model/*.class