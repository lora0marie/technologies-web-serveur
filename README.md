## Technologies Web Serveur

# Systéme de gestion de listes

Dans le dossier comme vous pouvez le constater il y a un fichier makefile.

grâce au fichier les commandes suivantes sont disponibles :

* make
* make run (lancer l'app)
* make clean (effacer les fichiers .class)


# l'architecture du projet

- README
- Makefile
- tp
    - Controleur.java
    - FreMarkerEngine.java ()
    - lib (élément dans l'archive zip fourni avec le tp)
    - model
        - Element.java
        - Liste.java
        - Model.java
        - Sql2oModel.java
    - resources
        - Liste.sql (fichier qui contient les requêtes permettant l'initialisation de la bdd ainsi que quelques exemples de remplissage)
        - ajouter-element.ftl
        - ajouter-liste.ftl
        - boostrap.min.css (biliothèque boostrap standard -> mise en forme simple pour l'affichage)
        - index.ftl
        - liste.ftl
        - modifier-element.ftl
        - modifier-liste.ftl